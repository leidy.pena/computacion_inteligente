potencia(0,0,'ERROR') :- !.
potencia(X,0,1) :- X =\= 0.
potencia(X,Y,Z) :- A is Y -1, potencia(X,A,B), Z is B*X.
