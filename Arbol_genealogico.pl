es_abuelo(terach,isaac).
es_abuelo(terach,lot).
es_abuelo(terach,milcah).
es_abuelo(terach,yiscah).

es_hermano(abraham,nachor).
es_hermano(abraham,haran).
es_hermano(haran,nachor).
es_hermano(lot,milcah).
es_hermano(lot,yiscah).
es_hermana(milcah,yiscah).

es_tio(abraham,lot).
es_tio(abraham,milcah).
es_tio(abraham,yiscah).
es_tio(nachor,lot).
es_tio(nachor,milcah).
es_tio(nachor,yiscah).
es_tio(nachor,isaac).
es_tio(haram,isaac).
es_tia(sarah,lot).
es_tia(sarah,milcah).
es_tia(sarah,yiscah).

es_primo(isaac,lot).
es_primo(isaac,milcah).
es_primo(isaac,yiscah).
es_prima(milcah,isaac).
es_prima(yiscah,isaac).

es_padre(terach,abraham).
es_padre(terach,nachor).
es_padre(terach,haran).
es_padre(abraham,isaac).
es_padre(haran,lot).
es_padre(haran,milcah).
es_padre(haran,yiscah).
es_madre(sarah,isaac).

es_hombre(terach).
es_hombre(abraham).
es_hombre(nachor).
es_hombre(haran).
es_hombre(isaac).
es_hombre(lot).
es_mujer(sarah).
es_mujer(milcah).
es_mujer(yiscah).

es_esposo(abraham,sarah).
es_esposa(sarah,abraham).

es_suegro(terach,sarah).
es_nuera(sarah,terach).

es_cuada(sarah,nachor).
es_cuada(sarah,haran).
es_cuado(nachor,sarah).
es_cuado(haram,sarah).
