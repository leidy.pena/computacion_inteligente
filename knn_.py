# -*- coding: utf-8 -*-
"""knn_

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1Vo-njXYFcfDI7IAA3pUFQHJd0_ZeugA9
"""

import pandas as pd 
import math as m
datos = pd.read_csv("iris.csv", header=None, skiprows=1)
x1 = []
x2 = []
x3 = []
x4 = []
etiqueta = []
distancia = []


def datos():
  for i in range(len(datos[0])):
    x1.append(datos[0][i])
  for i in range(len(datos[1])):
    x2.append(datos[1][i])
  for i in range(len(datos[2])):
    x3.append(datos[2][i])
  for i in range(len(datos[3])):
    x4.append(datos[3][i])
  for i in range(len(datos[4])):
    etiqueta.append(datos[4][i])
  
def distancia_euclideana(y1,y2,y3,y4):
  for i in range(len(x1)):
    opera = m.sqrt((x1[i]-y1)**2+(x2[i]-y2)**2+(x3[i]-y3)**2+(x4[i]-y4)**2)
    distancia.append(opera)
    
def orden(distancia, etiqueta):
    for numPasada in range(len(distancia)-1,0,-1):
        for i in range(numPasada):
            if distancia[i]>distancia[i+1]:
                temp = distancia[i]
                temp1= etiqueta[i]
                distancia[i] = distancia[i+1]
                etiqueta[i] = etiqueta[i+1]
                distancia[i+1] = temp
                etiqueta[i+1] = temp1


def n_vecinos(x):
  vecinos = []
  a= int(input("Ingrese el valor para k: "))
  for i in range(a):
    vecinos.append(x[i])
  return vecinos

def knn(x):
  con1 = 0
  con2 = 0
  con3 = 0
  for i in range(len(x)):
    if(x[i] == "Iris-setosa"):
      con1 = con1 + 1
    elif(x[i] == "Iris-versicolor"):
      con2 = con2 + 1
    elif(x[i] == "Iris-virginica"):
      con3 = con3 + 1
  print(con1)
  print(con2)
  print(con3)
  if(con1 > con2 and con1 >con3):
    return print("Iris-setosa")
  elif(con2 > con1 and con2 >con3):
    return print("Iris-versicolor")
  elif(con3 > con1 and con3 > con2):
    return print("Iris-virginica") 
  
def ejecutar():
  datos()
  distancia_euclideana(7.2,3.6,5.1,2.5)
  orden(distancia, etiqueta)
  v = n_vecinos(etiqueta)
  knn(v)
ejecutar()